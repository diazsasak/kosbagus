@extends('layout')
@section('judul')
Peta
@stop
@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset("dist/css/peta.css") }}">
@stop
@section('konten')
<input type="text" class="controls" id="searchTextField" placeholder="Masukkan alamat .." style="width: 50%;">
<div id="map"></div>
@stop
@section('script')
<!-- FastClick -->
<script src="{{ asset("bower_components/admin-lte/plugins/fastclick/fastclick.min.js") }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA25L7t2S05UTpLAaMDpv93se2EpoLWL_o&callback=init&region=ID&v=3.20&libraries=places&language=ID">
</script>
<script type="text/javascript">
//membuat map
var map,autocomplete;
function init() {
	initMap();
	initListener();
	initAutocomplete();
};

//buat marker
function createMarker(mapPos){
	var marker = new google.maps.Marker({
		position: mapPos,
		title: "Lokasi kos anda",
		draggable: true,
		animation: google.maps.Animation.DROP,
		map: map
	});
};

//buat peta
function initMap(){
	//map
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: -3.0926415, lng: 115.2837585},
		zoom: 5
	});
};

//listener untuk membuat marker baru
function initListener(){
	map.addListener( 'click', function(event){
		createMarker(event.latLng);
	});
};

//auto complete
function initAutocomplete(){
	var defaultBounds = new google.maps.LatLngBounds(
		new google.maps.LatLng(-33.8902, 151.1759),
		new google.maps.LatLng(-33.8474, 151.2631)
		);
	var input = document.getElementById('searchTextField');
	var options = {
		bounds: defaultBounds,
		types: ['address'],
		componentRestrictions: {country: 'id'}
	};
	autocomplete = new google.maps.places.Autocomplete(input, options);
	geolocate();
};

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate(){
	if(navigator.geolocation){
		navigator.geolocation.getCurrentPosition(function(position){
			var geolocation = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			var circle = new google.maps.Circle({
				center: geolocation,
				radius: position.coords.accuracy
			});
			autocomplete.setBounds(circle.getBounds());
			autocomplete.bindTo('bounds', map);
		});
	}
};
</script>
@stop