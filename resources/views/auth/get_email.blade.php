<!-- resources/views/auth/password.blade.php -->
@extends('layout')
@section('judul')
Lupa password
@stop
@section('konten')
<form method="POST" action="{!! route('post.email') !!}">
	{!! csrf_field() !!}
	<div>
		Email
		<input type="email" name="email" value="{{ old('email') }}" required>
	</div>
	<div>
		<button type="submit">
			Send Password Reset Link
		</button>
	</div>
</form>
@stop