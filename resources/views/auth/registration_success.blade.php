@extends('layout')
@section('judul')
Pendaftaran berhasil 
@stop
@section('konten')
Pendaftaran akun anda berhasil, <br/>
Kami telah mengirimkan email ke {!! $email !!}. <br/>
Harap klik tautan pada email tersebut untuk mengaktivasi akun anda.
@stop