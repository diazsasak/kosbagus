@extends('layout')
@section('judul')
Login
@stop
@section('konten')
<!-- resources/views/auth/login.blade.php -->
<form method="POST" action="login">
	{!! csrf_field() !!}
	<div>
		Email
		<input type="email" name="email" value="{{ old('email') }}">
	</div>
	<div>
		Password
		<input type="password" name="password" id="password">
	</div>
	<div>Authentication
		<input type="checkbox" name="remember"> Remember Me
	</div>
	<a href="{!! route('get.email') !!}">Lupa password</a>
	<div>
		<button type="submit">Login</button>
	</div>
</form>
@stop