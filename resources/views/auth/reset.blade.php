@extends('layout')
@section('judul')
Reset Password
@stop
@section('konten')
<form method="POST" action="/password/reset">
	{!! csrf_field() !!}
	<input type="hidden" name="token" value="{{ $token }}">
	<div>
		Password baru
		<input type="password" name="password">
	</div>
	<div>
		Konfirmasi password baru
		<input type="password" name="password_confirmation">
	</div>
	<div>
		<button type="submit">
			Reset Password
		</button>
	</div>
</form>
@stop