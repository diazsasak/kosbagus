@extends('layout')
@section('judul')
Daftar
@stop
@section('konten')

<!-- resources/views/auth/register.blade.php -->
<form method="POST" onsubmit="daftar.disabled=true; return true;" action="register">
	{!! csrf_field() !!}
		Nama
		<input type="text" name="nama" required> <br/>
		Foto profil
		<input type="file" name="foto"> <br/>
		Nomor Handphone
		<input type="number" name="no_hp" required> <br/>
		kontak lainnya
		<textarea name="kontak_lain">
			
		</textarea> <br/>
		Email
		<input type="email" name="email" required> <br/>
		Password
		<input type="password" name="password" required><br/>
		Confirm Password
		<input type="password" name="password_confirmation" required><br/>
		<button type="submit" name="daftar">Register</button>
</form>
@stop