<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Reset Password</title>
</head>
<body>
	Halo {{ $nama }}, <br><br>
	<br><br>
	Mohon klik link di bawah untuk mereset password anda
	{{ url('password/reset/'.$token) }}
</a><br><br><br><br><br>
Salam,<br><br>
<p style="color:red;">admin@kosbagus.com</p>
</body>
</html>