<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Verifikasi email</title>
</head>
<body>
	Halo {{ $name }}, Terima kasih sudah mendaftar :) <br><br>
	<br><br>
	Mohon klik link di bawah untuk memverifikasi registrasi anda
	<a href=”{{ url('/registration/activate'. $code) }}”></a>
	{{ url('/registration/activate/'. $code) }}
</a><br><br><br><br><br>
Salam,<br><br>
<p style="color:red;">admin@kosbagus.com</p>
</body>
</html>