<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use kosbagus\PasswordReset;

Route::get('/', [
	'as' => 'index', 'uses' => 'HomeController@index'
	]);

//Registrasi
Route::get('register', [
	'as' => 'register', 'uses' => 'Auth\AuthController@register'
	]);
Route::post('register', [
	'as' => 'post.register', 'uses' => 'Auth\AuthController@postRegister'
	]);
Route::get('/registration/activate/{code}', [
	'as' => 'activate', 'uses' => 'Auth\AuthController@activate'
	]);

//login
Route::get('login', [
	'as' => 'login', 'uses' => 'Auth\AuthController@login'
	]);
Route::post('login', [
	'as' => 'post.login', 'uses' => 'Auth\AuthController@postLogin'
	]);
Route::get('logout', [
	'as' => 'logout', 'uses' => 'Auth\AuthController@logout'
	]);

// Password reset link request routes...
Route::get('password/email', [
	'as' => 'get.email', 'uses' => 'Auth\PasswordResetController@getEmail'
	]);
Route::post('password/email', [
	'as' => 'post.email', 'uses' => 'Auth\PasswordResetController@postEmail'
	]);
// Password reset routes...
Route::get('password/reset/{token}', [
	'as' => 'get.reset', 'uses' => 'Auth\PasswordResetController@getReset'
	]);
Route::post('password/reset', [
	'as' => 'post.reset', 'uses' => 'Auth\PasswordResetController@postReset'
	]);

//map
Route::get('map',[
	'as' => 'map', 'uses' => 'MapController@map']
	);