<?php

namespace kosbagus\Http\Controllers;

use kosbagus\Http\Controllers\Controller;

class HomeController extends Controller {
	public function index(){
		return view('home.welcome');
	}
}