<?php

namespace kosbagus\Http\Controllers;

use kosbagus\User;
use kosbagus\Http\Controllers\Controller;

class MapController extends Controller {
	public function map(){
		return view('map');
	}
}