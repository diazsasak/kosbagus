<?php

namespace kosbagus\Http\Controllers\Auth;

use kosbagus\Http\Requests\ConfirmPassword;
use kosbagus\Http\Requests\ConfirmEmail;
use kosbagus\Http\Controllers\Controller;
use kosbagus\PasswordReset;
use kosbagus\User;
use Session;
use Mail;

class PasswordResetController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function getEmail(){
        return view('auth.get_email');
    }

    public function postEmail(ConfirmEmail $request){
    	$model = User::where('email', '=', $request->input('email'))->get(['nama']);
        if($model->isEmpty()){
        	Session::set('gagal','Maaf email tersebut tidak terdaftar');
        	return view('auth.get_email');
        	// var_dump($model);
        }
        else{
        	$pr = new PasswordReset;
           $pr->email = $request->input('email');
           $pr->token = str_random(60);
           $pr->save();
           Session::set('sukses','Link untuk reset password anda sudah dikirimkan ke email : '
              .$request->input('email'));
           $data = [
              'nama' => $model[0]->nama,
              'token' => $pr->token
           ];
           $this->sendEmail($data, $pr->email);
           return redirect()->route('index');
       }
    }

    public function sendEmail($data,$email){
      Mail::send('emails.password_reset', $data, function($message) use ($data,$email){
         $message->from('admin@kosbagus.com', 'KosBagus.com');
         $message->to($email, $data['nama'])->subject('Reset Password');
     });
    }

    public function getReset($token){
        $model = PasswordReset::where('token', $token)->get();
        if(!$model->isEmpty()){
            return view('auth.reset')->with('token',$token);
        }
        else{
            Session::set('gagal', 'Link reset password tidak valid');
            return view('home.welcome');
        }
    }

    public function postReset(ConfirmPassword $request){
        $model = PasswordReset::where('token',$request->input('token'))->get();
        if(!$model->isEmpty()){
            $password = bcrypt($request->input('password'));
            $user = User::where('email', $model[0]->email)->update(['password' => $password]);
            Session::set('sukses','password berhasil dirubah, silahkan login');
            return view('auth.login');
        }
        else{
            Session::set('gagal', 'Link reset password tidak valid');
            return view('home.welcome');
        }
    }

}
