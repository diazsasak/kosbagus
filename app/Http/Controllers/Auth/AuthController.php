<?php

namespace kosbagus\Http\Controllers\Auth;

use kosbagus\Http\Controllers\Controller;
use kosbagus\Http\Requests\Daftar;
use kosbagus\Http\Requests\Login;
use kosbagus\User;
use Mail;
use Auth;
use Session;

class AuthController extends Controller {
	public function register(){
		return view('auth.register');
	}

	public function sendEmail($data, $email){
		Mail::send('emails.account_activation', $data, function($message) use ($email, $data){
			$message->from('admin@kosbagus.com', 'KosBagus.com');
			$message->to($email, $data['name'])->subject('Mohon verifikasi registrasi akun anda');
		});
	}

	public function activate($code, User $user){
		if($user->activateAccount($code)){
			Session::set('sukses', 'Email berhasil diverifikasi, silahkan login');
			return redirect()->route('login');
		}
		else{
			Session::set('gagal', 'Link aktivasi tidak valid');
			return redirect()->route('index');
		}
	}
	public function postRegister(Daftar $request){
		$input = $request->all();
		$input['password'] = bcrypt($input['password']);
		$input['activation_code'] = str_random(60);
		$register = User::create($input);

		$data = [
			'name' => $input['nama'],
			'code' => $input['activation_code']
		];

		$this->sendEmail($data, $input['email']);
		$email = $input['email'];
		return view('auth.registration_success')->with('email',$input['email']);
	}

	public function postLogin(Login $request){
		// $credentials = $request->only('email', 'password', 'remember');
		if(Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')], $request->input('remember'))){
			if(Auth::user()->activation_code != NULL){
				Auth::logout();
				Session::set('gagal', 'Mohon verifikasi email anda');
				return redirect()->route('login');
			}
			else{
				Session::set('sukses', 'Login berhasil');
				return redirect()->route('index');
			}
		}
		else{
			Session::set('gagal', 'Email atau password anda salah');
			return redirect()->route('login');
		}
	}

	public function login(){
		return view('auth.login');
	}

	public function logout(){
		Auth::logout();
		Session::set('sukses', 'Anda telah logout');
		return redirect()->route('index');
	}
}