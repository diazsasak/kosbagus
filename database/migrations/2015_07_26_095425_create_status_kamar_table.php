<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusKamarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_kamar', function (Blueprint $table) {
            $table->integer('no_kamar')->primary();
            $table->integer('id_kos')->unsigned();
            $table->boolean('status')->default(false);
            $table->date('tanggal_habis_sewa');
            $table->timestamps();
            //foreign key
            $table->foreign('id_kos')->references('id_kos')->on('kos')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('status_kamar');
    }
}
