<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kos', function (Blueprint $table) {
            $table->increments('id_kos');
            $table->string('nama');
            $table->string('alamat');
            $table->string('biaya_air');
            $table->string('biaya_listrik');
            $table->string('sumber_air');
            $table->string('jenis_kamar_mandi');
            $table->string('dapur');
            $table->string('url_kos');
            $table->string('gender_kos');
            $table->boolean('tv');
            $table->boolean('lemari');
            $table->boolean('kasur');
            $table->boolean('cctv');
            $table->boolean('parkir_mobil');
            $table->boolean('parkir_motor');
            $table->string('luas_kamar');
            $table->longText('keterangan');
            $table->longText('peraturan');
            $table->integer('pemilik');
            $table->timestamps();
            //foreign key
            $table->foreign('id_kos')->references('id')->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kos');
    }
}
