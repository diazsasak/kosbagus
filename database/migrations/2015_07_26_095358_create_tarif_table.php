<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTarifTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarif', function (Blueprint $table) {
            $table->increments('id_tarif');
            $table->integer('id_kos')->unsigned();
            $table->integer('jumlah_penghuni');
            $table->integer('harian');
            $table->integer('bulanan');
            $table->integer('tahunan');
            $table->timestamps();
            //foreign key
            $table->foreign('id_kos')->references('id_kos')->on('kos')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tarif');
    }
}
