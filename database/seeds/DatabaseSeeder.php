<?php

use Illuminate\Database\Seeder;
use kosbagus\User;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('UserTableSeeder');
        $this->command->info('User table seeded!');

        Model::reguard();
    }
}

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $user = new User;
       $user->nama = "Dummy";
       $user->email = "dummy@dummy.com";
       $user->password = bcrypt("testing");
       $user->save();

       $user = new User;
       $user->nama = "asdf";
       $user->email = "diazfebrian2@gmail.com";
       $user->password = bcrypt("testing");
       $user->save();
    }
}

