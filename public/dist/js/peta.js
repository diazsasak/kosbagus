//membuat map
var map;
function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: -34.397, lng: 150.644},
		zoom: 8
	});
}
//listener untuk membuat marker baru
google.maps.event.addListener('click', function(event){
	createMarker(event.latLng);
});
//buat marker
function createMarker(mapPos){
	var marker = new google.maps.Marker({
		position: mapPos,
		title: "Lokasi kos anda",
		draggable: true,
		animation: google.maps.Animation.DROP
	});
};